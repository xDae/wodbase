var Vue = require('Vue');
var Firebase = require('firebase');
var GoogleMapsLoader = require('google-maps');
var _ = require('lodash');

var baseURL = new Firebase('https://wodbase.firebaseio.com');

var centers = new Firebase(baseURL + 'crossfitCenters');

var map;

centers.on('child_added', function (snapshot) {
    var item = snapshot.val();
    mapApp.boxList.push(item);
});


var styles = [{'featureType':'landscape','stylers':[{'saturation':-100},{'lightness':65},{'visibility':'on'}]},{'featureType':'poi','stylers':[{'saturation':-100},{'lightness':51},{'visibility':'simplified'}]},{'featureType':'road.highway','stylers':[{'saturation':-100},{'visibility':'simplified'}]},{'featureType':'road.arterial','stylers':[{'saturation':-100},{'lightness':30},{'visibility':'on'}]},{'featureType':'road.local','stylers':[{'saturation':-100},{'lightness':40},{'visibility':'on'}]},{'featureType':'transit','stylers':[{'saturation':-100},{'visibility':'simplified'}]},{'featureType':'administrative.province','stylers':[{'visibility':'off'}]},{'featureType':'water','elementType':'labels','stylers':[{'visibility':'on'},{'lightness':-25},{'saturation':-100}]},{'featureType':'water','elementType':'geometry','stylers':[{'hue':'#ffff00'},{'lightness':-25},{'saturation':-97}]}];

var marker;

var mapApp = new Vue({

	// element to mount to
	el: '#map',
	ready: function() {
        this.initMap();
        this.geolocation();
    },

	// initial data
	data: {
		boxList: [],
        markers: []
	},
    watch: {
        'boxList': 'addMarkers'
    },
	methods: {
        sayHi: function () {
            console.log('hola!');
        },
		initMap: function () {
            GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
            GoogleMapsLoader.load(function(google) {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {
                        lat: 53.539806,
                        lng: 9.990993
                    },
                    zoom: 3,
                    styles: styles
                });
            }, console.info('map loaded!'));

		},
        addMarkers: function () {
            var infowindow = new google.maps.InfoWindow();

            for (var i = 0; i < this.boxList.length; i++) {
                var box = this.boxList[i];

                marker = new google.maps.Marker({
                    position: {
                        lat: box.location.lat,
                        lng: box.location.lng
                    },
                    icon: 'src/assets/map-marker.png',
                    title: box.name,
                    map: map
                });

                marker.setMap(map);

                marker.addListener('click', function() {
                    infowindow.setContent(this.title);
                    infowindow.open(map, this);
                });
            }

            console.info('markers added!');
        },
        geolocation: function () {
            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition(function(position) {

                    var myLatlng = {lat: position.coords.latitude, lng: position.coords.longitude};
                    map.panTo(myLatlng);
                    map.setZoom(15);

                });
            } else {
              console.warn("Geolocation no soportado :-( ");
            }
        }
	}
});
