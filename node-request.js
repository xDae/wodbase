var request = require('request');
var Firebase = require('firebase');
var chalk = require('chalk');
var _ = require('lodash');

var boxToFirebase = {

    options: {
        wodbasedb: 'https://wodbase.firebaseio.com/',
        GoogleMapsApiKey: 'AIzaSyBnyom7Xv5U1wXW_oeGRBxgTmNmtq1n70E',
        boxListUrl: {
            url: 'https://map.crossfit.com/getAllAffiliates.php',
            // url: 'https://wodbase.firebaseapp.com/crossfitCenters.json',
            json: true
        }
    },
    addAllBoxes: function(db, options) {

        var db = db || new Firebase(this.options.wodbasedb);
        var options = options || this.options.boxListUrl;

        var callRequest = function(error, response, centersList) {
            if (!error && response.statusCode == 200) {

                var boxesPath = db.child('crossfitCenters');
                var counter = 0;

                var onComplete = function(error) {
                    if (error) {
                        console.log(chalk.red('✘ ') + ' Error');
                    } else {
                        console.log(chalk.green('✔ ') + ' ' + chalk.italic(counter) + ' - ' + centersList[i][2] + ' was added. '+ chalk.blue('(ID: ' + centersList[i][3] + ')'))
                    }
                };

                for (i = 0; i < centersList.length; i++) {

                    var name = centersList[i][2]
                    var boxId = centersList[i][3];
                    var lat = centersList[i][0];
                    var lng = centersList[i][1];

                    // Creating the Box ID name + position as Unique ID
                    var uniqueID = (_.camelCase(name + lat + lng)).toString();

                    var newPath = boxesPath.child(uniqueID);

                    counter++;

                    // push the info to Firebase
                    newPath.update({
                        _cfComId: Number(boxId),
                        name: name,
                        location: {
                            lat: lat,
                            lng: lng
                        }
                    }, onComplete());

                }

            } else {
                console.log(chalk.red('✘ Error madafaka!'));
                process.exit();
            }
        };

        // call the 'api' with the crossfit box list
        request(options, callRequest);
    },
    checkIfBoxExists: function (name) {
        var ref = new Firebase('https://wodbase.firebaseio.com/crossfitCenters/');

        ref.orderByChild('name').equalTo(name).once('value', function(snapshot) {

            var counter = 0;
            var ids = [];

            snapshot.forEach(function(childSnapshot) {
                if (name === childSnapshot.val().name) {
                    ids.push(childSnapshot.key());
                    counter++;
                }
            });

            if (counter === 0) {
                console.log(chalk.red('✘  ') + counter + ' results!');
                return false;
            } else {
                console.log(chalk.green('✔  ') + counter + ' results!');
                for (var i = 0; i < ids.length; i++) {
                    console.log(chalk.cyan('Box ID: ') + ids[i]);
                }
                return true;
            }
        });
    },
    addGplaceId: function (boxId) {
        var ref = new Firebase('https://wodbase.firebaseio.com/crossfitCenters/' + boxId);

        var GooglePlaceApiUrl = function (boxObject) {
            var boxName = boxObject.name;
            var lat = boxObject.location.lat;
            var lng = boxObject.location.lng;
            var apiKey = boxToFirebase.options.GoogleMapsApiKey;

            return 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+ lat +','+ lng +'&radius=100&name='+ boxName +'&key='+ apiKey;
        };

        ref.once('value', function(snapshot) {
            var boxKey = snapshot.ref();
            var boxData = snapshot.val();

            var options = {
                url: GooglePlaceApiUrl(boxData),
                json: true
            };

            request(options, function (error, response, responseData) {
                // Checks if the Box has a Google Place ID
                if (!snapshot.child('_googlePlaceID').exists()) {

                    if (!error && response.statusCode == 200) {
                        // check if the Box exists in the Google Places API
                       if (responseData.status === 'OK') {

                           var boxData = responseData.results[0];

                           boxKey.update({
                               '_googlePlaceID': boxData.place_id
                           }, onComplete(boxData));

                           var onComplete = function (data) {
                               console.log(chalk.green('✔  ') + data.name);
                               console.log(chalk.blue('Google Place ID: ') + data.place_id);
                               console.log(chalk.white('---------------'));
                           };

                        } else if (responseData.status === 'OVER_QUERY_LIMIT') {
                            console.log(chalk.red('✘ ' + 'Google API limits reached'));

                            // kill the node process if we have a OVER_QUERY_LIMIT from Google
                            process.exit();
                        }
                    }
               } else if (snapshot.child('_googlePlaceID').exists()) {
                   console.log(chalk.yellow('✔ ' + boxData.name + ' ya dispone de Google Places ID'));
               }
            });

        }, function (errorObject) {
            console.log('The read failed: ' + errorObject.code);
        });

    },
    addGInfo: function () {
        var ref = new Firebase('https://wodbase.firebaseio.com/crossfitCenters/');
        ref.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {

                var key = childSnapshot.key().toString();
                boxToFirebase.addGplaceId(key);
            });
        });
    }
};

// boxToFirebase.addAllBoxes();
boxToFirebase.addGInfo();
// boxToFirebase.checkIfBoxExists('Armor CrossFit');
